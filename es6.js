console.log("Nomor 1");


let golden = () => {
  return "this is golden!!"
}

console.log(golden());

console.log("Nomor 2");

var nama1 = "William"
var nama2 = "Imoh"


let newFunction = (a,b) => {
  return nama1 + " " + nama2
}

console.log(newFunction("William","Imoh"))

console.log("Nomor 3");

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}
const {firstName, lastName, destination, occupation, spell} = newObject

console.log(firstName, lastName, destination, occupation)

console.log("Nomor 4");

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [west, east]

console.log(combined)

console.log("Nomor 5");

const planet = "earth"
const view = "glass"
const lorem = "Lorem"
const dolor = "dolor sit amet, "
const consec = "consectetur adipiscing elit,"
const donat = "do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam"

const before = `${lorem} ${view} ${dolor} ${consec} ${planet} ${donat}`

console.log(before);
