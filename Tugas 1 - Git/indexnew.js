var readBooks = require('./callback.js')
var timeRead = 10000
var book = [
    {name: 'LOTR', timeSpent: 3000},
    {name: 'Fidas', timeSpent: 2000},
    {name: 'Kalkulus', timeSpent: 4000}
]

function countDown(x){
  if (x == book.length) {
    return 0;
  }
  readBooks(timeRead,book[x],function(callback) {
    timeRead = callback;
  })
  countDown(x+1);
}
countDown(0);
