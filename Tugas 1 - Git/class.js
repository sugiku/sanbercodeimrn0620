console.log("Nomor 1-Release 0");

class Sheep {
  constructor(animal) {
    this.name = animal;
    this.legs = 4
    this.cold_blooded = false
  }
}

sheep = new Sheep("Shaun");
console.log(sheep.name);
console.log(sheep.legs);
console.log(sheep.cold_blooded);

console.log("Nomor 1-Release 1");

class Ape {
  constructor(animal) {
    this.name = animal;
    this.legs = 2
    this.yell = "Auooo"
  }
}
class Frog {
  constructor(animal) {
    this.name = animal;
    this.legs = 4
    this.jump = "hop hop"
  }
}

sungokong = new Ape("kera sakti")
kodok = new Frog("buduk")

console.log(sungokong.yell);
console.log(kodok.jump);

console.log("Nomor 2");

class Clock {
        constructor({ template }){
        this.template = template;
        }
    render() {
    this.date = new Date();
    this.hours = this.date.getHours();
    if (this.hours < 10) hours = '0' + this.hours;
    this.mins = this.date.getMinutes();
    if (this.mins < 10) mins = '0' + this.mins;
    this.secs = this.date.getSeconds();
    if (this.secs < 10) secs = '0' + this.secs;
    this.output = this.template
      .replace('h',this.hours)
      .replace('m',this.mins)
      .replace('s',this.secs);
    console.log(this.output);
  }
  stop(){
    clearInterval(this.timer);
  }
  start(){
    this.render();
    this.timer = setInterval(() => this.render(), 1000);
  }
}



var clock = new Clock({template: 'h:m:s'});
clock.start();
