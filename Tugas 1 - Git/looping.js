console.log('-------------------------------');
console.log('Looping Pertama');
console.log('-------------------------------');

var number = 2
while (number < 21) {
  console.log(number + '- I love coding');
  number+=2;
}

console.log('-------------------------------');
console.log('Looping Kedua');
console.log('-------------------------------');

var number = 20
while (number > 0) {
  console.log(number + '- I will become a mobile developer');
  number-=2;
}

var angka = 21

for (var i = 1; i < angka; i++) {
  if (i%2==0) {
    console.log(i+' - Berkualitas')}
  else if (i%3==0 && i%2==1)
    console.log(i+' - I Love Coding')
  else
    console.log(i+' - Santai')


}

var size1 = 0

while (size1<4) {
  console.log("########");
  size1++
}

var tangga = 8
var kaki =""

for (var i = 0; i < tangga; i +=1) {
  console.log(kaki+="#");
}

var size = 8;
var board = "";

for (var y = 0; y < size; y++) {
  for (var x = 0; x < size; x++) {
    if ((x + y) % 2 == 0)
      board += " ";
    else
      board += "#";
  }
  board += "\n";
}

console.log(board);
