console.log("Nomor 1");

var people = [["Bruce", "Banner", "male", 1975],["Natasha", "Romanoff", "female"]]
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]

var array = ""
var obj = {}
var objek = {}

function arrayToObject(arr){
  var arrObj = []
    for (var i = 0; i < arr.length; i++) {
      var yr = arr[i][3]
      var age;
        if (!arr[i][3] || arr[i][3]>2020) {
          age = "Invalid Birth Year"
        } else {
          age = 2020 - yr
        }
      objek = {
        "firstName":arr[i][0],
        "lastName":arr[i][1],
        "gender":arr[i][2],
        "age":age
      }
      arrObj.push(objek)
    }
    return arrObj
}

console.log(arrayToObject(people));
console.log(arrayToObject(people2));

var object = {}
var barangHarga = [["Sepatu Staccatu",1500000],["Baju Zoro",500000],["Baju H&N",250000],["Sweater Uniklooh",175000],["Casing HP",50000]]

function shoppingTime(memberId, money) {
  var belanjaan = []
  var cash = money
  if (!memberId) {
    return "Mohon maaf, toko X hanya berlaku untuk member saja"
  } else if (cash < 50000){
    return "Mohon maaf, uang tidak cukup";
  }
  for (var i = 0; i < barangHarga.length; i++) {
    if (cash >= barangHarga[i][1]){
    belanjaan[i] = barangHarga[i][0];
    cash = cash-barangHarga[i][1];
}}
    object = {
      "memberId":memberId,
      "money":money,
      "listPurchased":belanjaan,
      "change":cash
  }
  return object
}



console.log(shoppingTime('82Ku8Ma742', 170000))
console.log(shoppingTime('1820RzKrnWn08', 2475000))
console.log(shoppingTime('', 2475000));
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime());

var penumpang = [['Dimitri','B','F'],['Icha','A','B']]
var ruteJalan = ['A','B','C','D','E','F']

function naikAngkot(arr){
  var angkat = []
  var ngangkot = {}
  for (var i = 0; i < penumpang.length; i++) {
    var tarifB = ruteJalan.indexOf(penumpang[i][2])
    var tarifA = ruteJalan.indexOf(penumpang[i][1])
    var selisih = tarifB - tarifA
    var tarif = selisih*2000
    ngangkot = {
      "Penumpang":penumpang[i][0],
      "NaikDari":penumpang[i][1],
      "Tujuan":penumpang[i][2],
      "Bayar": tarif
    }
    angkat.push(ngangkot)
  }
    return angkat
  }



console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
