console.log("Nomor 1");

function range (x, y){
var deret = []
  if (!x||!y) {
    return -1;
  }
  if (x<y)
    for (var i = x; i <= y; i++) {
    deret.push(i)
    }

  if (x>y)
    for (var i=x; i>=y;i--) {
    deret.sort(function(x,y){return y-x})
    deret.push(i)
    }
    return deret
}
console.log(range(1,10));
console.log(range(1));
console.log(range(11,18));
console.log(range(54,50));
console.log(range());

console.log("Nomor 2");

function rangeWithStep(x,y,z){
  var deret2 = []
  if (x<y)
    for (var i = x; i <= y; i+=z){
    deret2.push(i)
    }
  if (x>y)
    for (var i=x; i>=y;i-=z){
    deret2.sort(function(x,y,z){return y-z})
    deret2.push(i)
    }
    return deret2
}
console.log(rangeWithStep(1,10,2));
console.log(rangeWithStep(11,23,3));
console.log(rangeWithStep(5,2,1));
console.log(rangeWithStep(29,2,4));

console.log("Nomor 3");

function ranged(x,y,z){
  var deret3 = []
  if (x==1 && y==0){
      return 1;
  }
  if (x<y)
    for (var i = x; i <= y; i+=z){
    deret3.push(i)
    }
  if (x>y)
    for (var i=x; i>=y;i-=z){
    deret3.sort(function(x,y,z){return y-z})
    deret3.push(i)
    }
    return deret3
}

function sum(ranged){
  if (ranged ==1) {
    return 1;
  } return ranged.reduce((x,y)=>x+y,0)
}

console.log(sum(ranged(1,10,1)));
console.log(sum(ranged(5,50,2)));
console.log(sum(ranged(15,10,1)));
console.log(sum(ranged(1,0,1)));
console.log(sum(ranged()));

console.log("Nomor 4");

var input = [
                ["0001"], ["Roman Alamsyah"], ["Bandar Lampung 21/05/1989"], ["Membaca"],
                ["0002"], ["Dika Sembiring"], ["Medan 10/10/1992"], ["Bermain Gitar"],
                ["0003"], ["Winona"], ["Ambon 25/12/1965"], ["Memasak"],
                ["0004"], ["Bintang Senjaya"], ["Martapura 6/4/1970"], ["Berkebun"]
            ]
var tambahan = [
                  ["Nomor ID: "],["Nama Lengkap: "],["TTL: "],["Hobi: "],["Nomor ID: "],["Nama Lengkap: "],
                  ["TTL: "],["Hobi: "],["Nomor ID: "],["Nama Lengkap: "],["TTL: "],["Hobi: "],["Nomor ID: "],["Nama Lengkap: "],
                  ["TTL: "],["Hobi: "]
                ]

function dataHandling() {
  var nama = []
      for (var i = 0; i < input.length; i++) {
      nama[i] = tambahan[i] + input[i];
}
      return nama
}
      console.log(dataHandling());

console.log("Nomer 5");

function balikkata(str){
  var string = " "
  for (var i = str.length - 1; i >= 0; i--) {
  string += str[i]
}
  return string
}

console.log(balikkata("Kasur Rusak"));
console.log(balikkata("SanberCode"));
console.log(balikkata("Haji Ijah"));
console.log(balikkata("racecar"));
console.log(balikkata("I am Sanbers"));

console.log("Nomor 6");


  var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]
  input.splice(0,5)
  input.push("0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro")
  var tgl = input[0,3]
  var tglSplit = tgl.split("/")
  var tglReal = tglSplit.map(Number)
  var mm = tglReal[0,1]
  var tglJoin = tglSplit.join("-")
  var namaPjg = input[0,1]
  var namaPdk = namaPjg.slice(0,14)


  console.log(input);
  switch (mm) {
  case 01:
    console.log("Januari");
    break;
  case 02:
    console.log("Februari");
    break;
  case 03:
    console.log("Maret");
    break;
  case 04:
    console.log("April");
    break;
  case 05:
    console.log("Mei");
    break;
  case 06:
    console.log("Juni");
    break;
  case 07:
    console.log("Juli");
    break;
  case 08:
    console.log("Agustus");
    break;
  case 09:
    console.log("September");
    break;
  case 10:
    console.log("Oktober");
    break;
  case 11:
    console.log("November");
    break;
  case 12:
    console.log("Desember");
    break;
  }
  tglReal.sort()
  console.log(tglReal);
  console.log(tglJoin);
  console.log(namaPdk);
