var word='JavaScript';
var second='is';
var third='awesome';
var fourth='and';
var fifth='I';
var sixth='love';
var seventh='it!';

var kalimat = word.concat(" ",second," ",third," ",fourth," ",fifth," ",sixth," ",seventh);

console.log(kalimat)

var sentence = "I am going to be React Native Developer";
var exampleFirstWord = sentence[0] ; 
var SecondWord = sentence[2] + sentence[3]  ;
var ThirdWord = sentence.substr(5, 6);
var FourthWord = sentence[11]+sentence[12];
var FifthWord = sentence[14]+sentence[15];
var SixthWord = sentence.substr(17, 5);
var SeventhWord = sentence.substr(30, 38);

console.log('First Word: ' + exampleFirstWord);
console.log('Second Word: '+ SecondWord);
console.log('Third Word: '+ ThirdWord);
console.log('Fourth Word: '+ FourthWord);
console.log('Fifth Word: '+ FifthWord);
console.log('Sixth Word: '+ SixthWord);
console.log('Seventh Word: '+ SeventhWord);

var sentence2 = 'wow JavaScript is so cool'; 
var exampleFirstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substr(4, 10);
var thirdWord2 = sentence2.substr(15, 2);
var fourthWord2 = sentence2.substr(18, 2);
var fifthWord2 = sentence2.substr(21, 5);

console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);
