import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { Platform, StyleSheet, Text, View, Image, ImageBackground, TouchableOpacity, TextInput } from 'react-native';
import Icons from 'react-native-vector-icons/MaterialIcons';

export default function App() {
  return (
    <View style={styles.container}>
    <ImageBackground source={require('./assets/BG2.png')} style={styles.image}>
      <View style={styles.header}>
      <Image source={require('./assets/Welcome.png')}/>
      </View>
        <View style={styles.display}>
        <Image source={require('./assets/MainPic.png')} style={{width:339, height:418}}/>
        </View>
      <View style={styles.display}>
      <Image source={require('./assets/edit.png')} style={{width:320, height:51}}/>
      </View>
    </ImageBackground>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center"
  },
  header: {
    paddingHorizontal: 20,
    flexDirection:'column',
    alignItems:'flex-start',
    justifyContent:'flex-start',
  },
  display: {
    marginTop: 30,
    alignItems:'center',
    flexDirection:'column',
  },
});
