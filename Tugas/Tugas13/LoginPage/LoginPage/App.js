import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { Platform, StyleSheet, Text, View, Image, ImageBackground, TouchableOpacity, TextInput } from 'react-native';
import Icons from 'react-native-vector-icons/MaterialIcons';

export default function App() {
  return (
    <View style={styles.container}>
      <ImageBackground source={require('./assets/BG.png')} style={styles.image}>
        <View style={styles.header}>
        <Image source={require('./assets/PORTFOLIO.png')}/>
        </View>
        <View style={styles.display}>
        <Image source={require('./assets/OpenDoodles.png')} style={{width:167, height:89}}/>
        </View>
            <View style={styles.loginBox}>
              <View style={styles.invisiBox}>
                <View style={styles.leftNavA}>
                <Icons style={styles.navIconA} name="person-outline" size={25}/>
                <TextInput style={styles.textBox} placeholder=" Email" placeholderTextColor = "grey"/>
                </View>
                <View style={styles.leftNavB}>
                <Icons style={styles.navIconA} name="lock" size={25}/>
                <TextInput style={styles.textBox} placeholder=" Password" placeholderTextColor = "grey"/>
                </View>
                <View style={styles.buttonArea}>
                <Text style={styles.textOff}>forgot password?</Text>
                <Image source={require('./assets/SignIn.png')} style={{width:100, height:32}}/>
              </View>
            </View>
        </View>
      </ImageBackground>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center"
  },
  header: {
    paddingHorizontal: 25,
    marginBottom: 20,
    flexDirection:'column',
    alignItems:'flex-start',
    justifyContent:'space-between',
  },
  display: {
    marginTop: 80,
    flexDirection:'row',
    justifyContent: 'space-evenly',
  },
  loginBox: {
    justifyContent:'space-between',
    width: 339,
    height: 140,
    marginLeft:11,
    marginRight:30,
    marginTop:25,
    marginBottom:145,
    backgroundColor:'white',
    borderRadius:45,
    borderWidth: 1,
    borderColor: 'white',
  },
  invisiBox: {
    flexDirection:'column',
    justifyContent:'center',
    marginTop:-18,
    marginLeft:-10
  },
  leftNavA: {
    flexDirection:'row',
    alignItems:'baseline',
    justifyContent:'space-evenly',
    marginBottom:-40,
    marginRight:30
  },
  leftNavB: {
    flexDirection:'row',
    alignItems:'baseline',
    justifyContent:'space-evenly',
    marginBottom:50,
    marginRight:30
  },
  textBox:{
    elevation:3,
    flexDirection:'row',
    alignItems:'center',
    width: 213,
    height: 33,
    backgroundColor:'#f2f2f2',
    borderRadius:5,
    borderWidth: 1,
    borderColor: '#f2f2f2',
  },
  navIconA: {
    flexDirection:'column',
    justifyContent:'flex-start',
    marginTop:60,
    marginLeft:30
  },
  textOff: {
    fontSize: 11,
    color: '#3c3c3c',
    marginLeft:30,
    marginTop:10,
    flexDirection:'column',
    alignSelf:'baseline'
  },
  buttonArea: {
    paddingHorizontal: 15,
    flexDirection:'row',
    alignItems:'stretch',
    justifyContent:'space-between',
  }
});
